# ANN Speedrunning

Artificial Neural Networks are powerful tools that are conceptually simple, but can be daunting to implement.
This is in part because building a robust data pipeline is complex, and also due to the immense variety of ways
that an artificial neural network can built, initialized, and trained.

The goal of this project is to further my knowledge in ANN implementation by applying strict time constraints to my attempts at finishing tasks, while simultaneously serving as a record of what I did for each task as well as why I did each task, both for my own sake and for the sake of anyone interested.

If you are discovering this project and want to further your own knowledge in ANNs, I reccomend learning the basics of ANNs, python, and statistics using other available tools. Some of my preferred resources, in no particular order:

[Youtube videos on ANNs](https://www.youtube.com/watch?v=aircAruvnKk)

[Stanford Class on Tensorflow 1](https://web.stanford.edu/class/cs20si/syllabus.html)

[Youtube videos on python](https://www.youtube.com/user/sentdex)

## Getting Started

Async and Tensorflow don't exactly play nicely with eachother, so I'm using two seperate environments- one for scraping and a second for machine learning.  Each has their own requirements.txt so you know what to pip install.
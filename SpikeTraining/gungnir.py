"""Base library for gungnir- aka gungneuro, a spiketrain neural network library built on asyncio
"""
import asyncio
import time
import collections

async def waiter(event):
	await event.wait()

class stimulus:
	"""A stimulus provides an input to gungneurons. The stimulus can either be constant or a
	function, depending on your needs
	"""
	def __init__(self, stimulus_type='constant', voltage=1.0):
		self.stimulus_type=stimulus_type
		self.voltage=voltage

class output:
	"""An output function- often a list of classifiers in the supervised case
	"""
	def __init__(self, output_type='raw', voltage=0.0):
		self.output_type=output_type
		self.voltage=voltage

class gungneuron:
	"""The base, dynamic, asynchronous spiking neuron. All neurons have a rest voltage of 0.0;
	the voltage parameter is the dynamic, actual voltage in the neuron at a given time.
	Connections are two way-send_spike needs to deliver a spike forward, but can also modify the
	neuron that sent it after the spike, in the same manner as organic synaptic plasticity
	"""
	def __init__(self, voltage=0.0, threshold=40.0, refractory_period=0.3, decay=500,
		rest_voltage=0.0, neuron_type='normal'):
		
		self.rest_voltage = rest_voltage
		self.voltage = voltage
		self.threshold = threshold
		self.refractory_period = refractory_period
		self.decay = decay
		self.neuron_type = neuron_type
		self.outputs = {}
		self.inputs = []
		self.refracting = False
		#self.event = asyncio.Event()
		self.event = None
		#self.started = asyncio.Event()
		#self.output_spike_event = asyncio.create_task(waiter(self.event))
		self.running = False
		self.spike_delay_time=0.0
		self.start_time=0.0
		self.event_task_created = False
	
	def connect_as_input(self, input_gungneuron, input_spike_voltage):
		"""Creates a one-way connection using the passed neuron as input and main neuron as output
		"""
		self.inputs.append([input_gungneuron, input_spike_voltage])

	def connect_as_output(self, output_gungneuron, output_spike_voltage):
		"""Creates a one-way connection using the passed neuron as output and main neuron as input
		"""
		self.outputs.update(output_gungneuron, output_spike_voltage)

	async def sleep_then_cancel(self, gathered_tasks, timeout=10.0):
		"""Takes a gathered tasklist, and schedules them for completion conccurently while waiting
		for a timeout period. Then cancels the gathered tasks
		"""
		await asyncio.sleep(timeout)
		gathered_tasks.cancel()
		return True

	async def listen_inputs(self, input_spike_event, input_spike_voltage):
		"""This is the awaited method that recieves input spikes while running
		"""
		print('listening: ', self)
		if self.neuron_type == 'normal':
			while self.running: #Consider making while True
				await input_spike_event.wait()
				await self.modulate(input_spike_voltage, spike_time=time.time()-self.spike_delay_time)
				self.spike_delay_time=time.time()
			return False #Should be cancelled before reaching this point
		elif self.neuron_type == 'constant_stimulus':
			while self.running:
				await self.modulate(input_spike_voltage, spike_time=0.0)
			return False

	async def modulate(self, input_spike_voltage, spike_time):
		"""Add an input spike to the main neuron voltage & subtract the delay from the previous 
		voltage	Does nothing if neuron is in refractory period. Will update later for backprop.
		we only need to	check the voltage after adding a spike, hence its place here.
		"""
		if not self.refracting:
			self.voltage = max(0.0, self.voltage - spike_time * self.decay) + input_spike_voltage
			if self.voltage >= self.threshold:
				self.send_spike()
				await asyncio.sleep(self.refractory_period)
				self.refracting = False
			return True
		return False

	def send_spike(self):
		"""The awaited method for delivering the output spike. Can probably be cleaned up to
		minimize boolean checks, will look at again later
		"""
		#if self.neuron_type != 'constant_stimulus':
		print('time and type and address: ',time.time()-self.startime, self.neuron_type, self)
		self.refracting = True #Call first to make approximately atomic- may be redundant
		self.event.set()
		self.event.clear()#TODO - verify this still spikes all outputs
		self.voltage = self.rest_voltage
		#self.startime=time.time()#Hacky, temporary solution
		return True

	async def gungnir_run(self, timeout=10.0):
		"""The base function that runs all neuron functions. Listens to all inputs, processes 
		spikes from those inputs, and processes spikes too outputs when those occur. Refractory
		periods are	handled elsewhere so that the neuron can always be listening- important for
		synaptic plasticity
		"""
		if not self.event_task_created:
			self.event = asyncio.Event()
			self.event_task_created = True
		self.running = True
		self.startime = time.time()
		self.spike_delay_time=time.time()
		task_list = []
		if self.neuron_type == 'normal':
			for pair in self.inputs:
				if not pair[0].event_task_created:
					pair[0].event = asyncio.Event()
					pair[0].event_task_created = True
				task_list.append(self.listen_inputs(input_spike_event=pair[0].event, 
						input_spike_voltage=pair[1]))
		elif self.neuron_type == 'constant_stimulus':
			task_list.append(self.listen_inputs(input_spike_event=None,
					input_spike_voltage=self.threshold))
		groupinputs = asyncio.gather(*task_list)
		run_canceller = self.sleep_then_cancel(groupinputs, timeout=timeout)
		overall_runner = asyncio.gather(groupinputs, run_canceller)
		await overall_runner
		self.running = False
		return True
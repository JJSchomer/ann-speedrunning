# Spike Training

Two of the primary differences between modern ANNs and brains are the actual dynamics, and discretization. Most ANNs are firing rate approximations of neurons, and thus only have represent the average steady state of the network for a given set of constant stimuli. Real neural learning is a dynamic process with changing inputs. This means taking the inputs constantly, and simulating the real dynamics of neuron spiking if we wish to do it on a machine.

The second aspect, the discretization, is less obvious. Since we're now interested in letting a network evolve over time, we are limited by the clock speed of our machine. Since real neurons are locally regulated, i.e. since the firing of neurons is based on the local release of neurotransmitters, they aren't limited by a local processor. The effects of this aren't totally obvious, and will be expanded upon later. Our initial attempts will try to get around this by modeling the dynamics in a 'firing stack' method if possible, but the math isn't clear.

## Crytographic Learning

One element here that I would like to flesh out is the cryptographic representation of our problems- in the case of addition, we have perhaps the simplest possible cypher, where we take in a number of keys, and the passphrase is simply the numerical combination of those keys. It sounds strange to describe atomic math operations as cryptographic cyphers, but it does give us a simple framework for problem description. At the time of this writing, I am not much of an expert in cryptography, so this section simply stands as a placeholder.

## Asynchronous Neurons

In order to model spike trains efficiently, we'll need to develop a model that operates efficiently. Since we are walking into each simulation without explicit knowledge of how the system will evolve (we anticipate that problem to be intractable), we will design each neuron to operate using the python async library. Advanced implementations of async neurons will attempt to 'speed up' calculations where appropriate- more on that later.

The lack of implicit physics makes implementing decay into the system somewhat difficult- whereas voltage leaks naturally out of neurons, we're forced to implement either decay in a discrete fashion. Arguably the ability to maintain charge without decay could prove useful down the road, but for now we try to approximate hebbian learning as much as possible.

To minimize operations, we keep a rolling clock for voltage updates. This is a buffer that contains the most recent inputs as well as the time recieved for each input.

## Efficient Encoding

We want to build a learning model that evolves to efficiently represent different problems, and we will score both the end-result, training time, and amount of required samples in terms of describing our networks. This means both adding more neurons to not-descriptive enough models, and removing redundant data in cases where the initial network has grown too deep or connected.

## Update Rule

Since we won't be making the simplifications necessary in the model to use a simple cost function, we need to develop an update rule ourselves. The update rule should be capable of converging towards a problem solution, should one exist, and in an ideal case should also penalize model complexity. After all, a general algorithm must balance network size, network training, and network accuracy.  The obvious latent question/implication is whether or not Hebbian learning is sufficient to cover all three of these parameters in a dependant manner.

The rules for hebbian learning are simple- an input spike that precedes an output spike is rewarded, and an input spike that follows an output spike is penalized. This covers the parameter of 'network size' by giving us a method for pruning or building connections in the problem space. The second aspect of this rule is the timing- we only reward/penalize spike pairs that fall within a certain timeframe of the output spike, and we do so in an negative inverse exponential matter. This gives us a couple of things- firstly, there is always some benefit to preempting the spike, but its greatly biased towards spikes that push the output over the threshold. Second, it automatically prunes redundant information. 

In terms of implementation, we want to retain each input for some amount of time. We only want to adjust weights within a window, so we can flush the buffer periodically until a spike fires. After the spike fires, we backpropogate a parameter that increases the spike magnitude of the input, in proportion to the time of spike. For a short time (at least the refractory period?!), we penalize the input spike magnitude in proportion to the time of spike.

## Gungneuron, first implementation

Each gungneuron has some associated parameters (spike threshold, refractory period, decay, etc), as well as
pointers to connected neurons. The inputs pointer allows the gungneuron to listen to all inputs for spiking, and the outputs pointer tells it both where to send a spike and what value to send along that connection (the values that are massaged during learning are these spike values, rather than other values). Connections are all two way- once a spike fires towards an output, it listens briefly to see if the output target itself fires or has fired recently. If so, it modifies its behavior.

## Example

Imagine we have a pair of input neurons that give 80mv and 60 mv respectively to the main neuron when presented with a stimulus. The main neuron has a resting voltage of 0 and a refractory period of 0.3 seconds. Imagine four scenarios- the first being that the neuron fires at 40 mv, the second at 70 mv, the third at 120 mv, and the fourth at 160 mv.  The 80 mv spike input neuron fires once a second, and the 60 mv spike input neuron fires 5.2 times per second. Lastly, the voltage in the main neuron decays at a rate of 500 mv/second. (This is only a small subset of possibilities, meant only to illustrate some possible trends)

In the first case, both neurons cause a spike in the main neuron as long as the refractory period isn't active. We'll build these using gungneurons and run them through a series of tests

Given 100 seconds of modeling, we can expect the following behavior:
100 input spikes from input 1, 520 input spikes from input 2, 
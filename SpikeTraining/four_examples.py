"""examples taken from the README to demonstrate hebbian plasticity, uses two input neurons with
constant input stimuli to drive a single main neuron
"""
import asyncio
import gungnir

main_neuron=gungnir.gungneuron(voltage=0.0, threshold=40.0, refractory_period=0.3, decay=500.0)

input_neuron_one=gungnir.gungneuron(voltage=1.0, threshold=1.0, refractory_period=1.0, decay=500.0,
		rest_voltage=1.0, neuron_type='constant_stimulus')

input_neuron_two=gungnir.gungneuron(voltage=0.0, threshold=1.0, refractory_period=0.1923,
		decay=500.0, rest_voltage=1.0, neuron_type='constant_stimulus')

main_neuron.connect_as_input(input_neuron_one, 80.0)
main_neuron.connect_as_input(input_neuron_two, 40.0)

async def main():
	await asyncio.gather(
		main_neuron.gungnir_run(),
		input_neuron_one.gungnir_run(),
		input_neuron_two.gungnir_run(),
		)

asyncio.run(main())
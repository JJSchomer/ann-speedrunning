import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
import time 
import random
import tensorflow as tf

import tensorflow.keras.backend
from tensorflow.keras import Sequential
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dense
#from tensorflow.keras.callbacks import TensorBoard
from tensorflow.keras.utils import Sequence
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.models import model_from_json
from datetime import datetime
import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import h5py
import sys
import xlsxwriter
import gc
config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
plt.tight_layout()


"""
StepGenerator is a bit different than NameGenerator because we want there to be an association
with the actual recipe name and the steps, as well as begin/terminate recipes properly.

The easiest way to do that is to include the recipe name alongside the step data as a parameter
that gets passed along a different network- ensuring that we continue to generate steps according
to the rules developed by the larger corpus. Thus each training input looks like this:

___text__of__current__recipe + __name__of__recipe__

We could try to train individual steps, but it isn't clear that there is an inherent advantage. We'll
revisit that later.

"""

class StepGenerator(Sequence):
	
	"""
	By padding the input, we should be able to start learning to generate concise recipe names.

	What actually happens is that we create a significant bias towards spaces. One way to combat
	this is to artificially truncate our data. We want the same step size, but we don't want a bunch
	of empty spaces polluting the trainability, so we can wrap text instead.
	"""

	def __init__(self, file_name='EncodedSteps.h5', batch_size=50, start_Index=0,
					 end_Index=None, PaddingLength=50):
		self.file_name=file_name
		self.char_names_indices = {}
		self.indices_names_char = {}

		with h5py.File('EncodedSteps.h5', 'r') as hf:
			for attribute in hf['OneHotSteps'].attrs:
				self.char_names_indices[str(attribute)]=hf['OneHotSteps'].attrs[str(attribute)]
				self.indices_names_char[hf['OneHotSteps'].attrs[str(attribute)]]=str(attribute)
			self.truelen = len(hf['OneHotSteps'][:])*10
			self.NamesLen = len(hf['OneHotSteps'][0])#check
		
		self.batch_size = batch_size
		print('NamesLen ',self.NamesLen)
		self.PaddingLength=PaddingLength

		if end_Index==None:
			self.total_len=self.truelen - start_Index*batch_size
			self.total_len=self.total_len-self.total_len%batch_size
		else:
			self.total_len = end_Index*batch_size - start_Index*batch_size
		self.idx = start_Index
		
		#A bit sloppy but works
		self.char_names_indices.pop('title')
		self.indices_names_char.pop('EncodedRecipeSteps from Food.com')
		self.Charlength=len(self.char_names_indices)
		print('Charlength: ',self.Charlength)

	def __len__(self):
		return int(self.total_len//self.batch_size)

	def __getitem__(self, idx):
		"""
		To fix this, we can a modulo term to cut batch sizing by approximately a factor of 10.

		We simply cut the [i] value from hf['OneHotSteps'] by //10 and appropriately up the j term.

		in practice looking at a 3x reduction in batch size:

			idx = 0,1,2,3,4,5,6

			i_0   = 0,0,0,1,1,1,2
			
			j_0 =  0,200,400,0,200,400,0

		We'll have to do fixing too to get all the data correctly, but that's relatively trivial (scaling length earlier does it).

		There's definitely a lesson here in the dangers of prepartitioning- as a better rule of thumb try to shoot for batches of either 10 or 100 so that you don't have to do this later.
		"""

		mod = 0
		if self.idx!=0:
			mod = self.idx
		#Don't modify this since

		modfix=10
		Totalchunksize=2000-self.PaddingLength-1
		targetchunksize=Totalchunksize//modfix//3 #cutting training time a bit with this, take it or leave it imho
		
		"""
		We want to cut the existing slices into 10 slices of data (of 65 x,y pairs):
		Easiest way to do this is to integer divide the index by 10 so we access the same slice repeatedly during getitem,
		followed by a call to the j range function that only grabs the dataset of interest
		"""

		batch_y=[]
		batch_x=[]

		with h5py.File('EncodedSteps.h5', 'r') as hf:
			for i in range( (idx+mod)//modfix, (idx+mod)//modfix+1) :
				for j in range(targetchunksize*((idx+mod)%modfix),targetchunksize*((idx+mod)%modfix)+targetchunksize):
					batch_x.append(hf['OneHotSteps'][i][j*3:j*3+self.PaddingLength])
					batch_y.append(hf['OneHotSteps'][i][j*3+self.PaddingLength])
					
			
		batch_y=np.array(batch_y)
		batch_x=np.array(batch_x)

		#print('Size batch_x: ',batch_x.size)
		#print('Size batch_y: ',batch_y.size)

		return(batch_x, batch_y)
	

def StepRNNCreation(LSTM_number=2000, Input_number=50, Learning_rate=0.01, end_Index=2000, train_epochs=1, start_Index=0):

	tf.keras.backend.clear_session()#Important to have this here not at the top
	
	"""
	logdir = os.path.join(
			"logs",
			"scalars",
			datetime.now().strftime("%Y%m%d-%H%M%S"),
			)
	tensorboard_callback = TensorBoard(log_dir=logdir, write_graph=True)
	"""

	char_names_indices = {}
	indices_names_char = {}

	with h5py.File('EncodedSteps.h5', 'r') as hf:
		for attribute in hf['OneHotSteps'].attrs:
			char_names_indices[str(attribute)]=hf['OneHotSteps'].attrs[str(attribute)]
			indices_names_char[hf['OneHotSteps'].attrs[str(attribute)]]=str(attribute)

	char_names_indices.pop('title')
	indices_names_char.pop('EncodedRecipeSteps from Food.com')

	print('Charinds: ',char_names_indices)
	print('IndChars: ',indices_names_char)

	batch_size=1#Hoisted by my own petard
	
	RecipeNames=StepGenerator(file_name='EncodedSteps.h5', PaddingLength=Input_number, batch_size=batch_size, start_Index=start_Index, end_Index=end_Index)

	Namelength=RecipeNames.NamesLen
	Charlength=RecipeNames.Charlength
	
	#LSTMs are hungry- don't go overboard here
	model = Sequential()
	model.add(LSTM(LSTM_number, batch_input_shape=(((2000-Input_number-1)//10//3), Input_number, Charlength), stateful=True))
	model.add(Dense(Charlength, activation='softmax'))

	try:
		print('attempting to load weights')
		model.load_weights('model.h5')
		print('load successful')
	except:
		print('no weights to load')
		pass

	#modelstring='LSTM'+str(LSTM_number)+'LR'+str(Learning_rate)+'IN'+str(Input_number)+"model"
	modelstring='currentModel'
	workbook=xlsxwriter.Workbook(modelstring+'.xlsx')
	worksheet=workbook.add_worksheet()
	worksheet.write('A1','Loss')
	worksheet.write('B1','epoch')

	optimizer = RMSprop(learning_rate=Learning_rate)
	model.compile(loss='categorical_crossentropy', optimizer=optimizer)

	#print_callback = LambdaCallback(on_epoch_end=on_epoch_end)

	history=model.fit(RecipeNames,
			epochs=train_epochs,
			shuffle=False,
			steps_per_epoch = None,
			use_multiprocessing = False,
			verbose=1,
			max_queue_size=100,
			workers=10,
			initial_epoch=0)
	print('history: ',history.history)
	print('saving weights')
	
	dirname='PostProcessing'
	try:
		os.mkdir(dirname)
	except:
		pass
	os.chdir(dirname)

	dirname2=modelstring
	#dirname2='currentModel'
	try:
		os.mkdir(dirname2)
	except:
		pass
	os.chdir(dirname2)

	model.save_weights(modelstring+str(start_Index)+".h5")
	print('model saved to disk')

	row=1
	for lossval in history.history['loss']:
		worksheet.write(row, 0, lossval)
		worksheet.write(row, 1, row)
		row+=1
	worksheet.write('C1','CurrentStartIndex')
	worksheet.write('C2' ,start_Index)

	workbook.close()

	plt.plot(history.history['loss'])
	plt.xlabel('Epochs')
	plt.ylabel('Loss')
	plt.title('Loss Per Epoch')
	plt.savefig(modelstring+'loss.png')
	plt.close()
	
	os.chdir('..')
	os.chdir('..')

	model.save_weights("model.h5")
	print('model saved to train location')



	del model
	del RecipeNames
	gc.collect()
	

	tf.keras.backend.clear_session()
	tf.compat.v1.reset_default_graph()



def sample(preds, temperature=1.0):
	# helper function to sample an index from a probability array
	preds = np.asarray(preds).astype('float64')
	preds = np.log(preds) / temperature
	exp_preds = np.exp(preds)
	preds = exp_preds / np.sum(exp_preds)
	probas = np.random.multinomial(1, preds, 1)
	return np.argmax(probas)


def on_epoch_end(epoch, _):
	#Most of text generation is handled in a different script
	
	
	print()
	print('----- Generating text after Epoch: %d' % epoch)

	for textgenCount in range(1):
		print('TextGen #: ',textgenCount)
		with h5py.File('EncodedSteps.h5', 'r') as hf:
			d1_names=hf['OneHotSteps']
			start_index = random.randint(0, len(d1_names))

			sentence1=d1_names[start_index]
			print('sentence1: ',len(sentence1),len(sentence1[0]))
			sentence=''

			for t in range(len(sentence1)):
				for j in range(len(sentence1[t])):
					if sentence1[t][j]==True:
						sentence=sentence+indices_names_char[j]


		for diversity in [0.1, 0.5, 0.8]:
			print('----- diversity:', diversity)

			generated = ''
			
			generated += sentence
			print('----- Generating with seed: "' + sentence + '"')
			sys.stdout.write(generated)

			for i in range(Input_number):
				
				x_pred = np.zeros((1, Input_number, len(char_names_indices))) 
				
				while len(sentence)<Input_number:
					sentence=sentence+' '+sentence

				for t in range(Input_number):
					x_pred[0,t,char_names_indices[sentence[t]]]= True
				
				preds = model.predict(x_pred, verbose=0)[0]
				next_index = sample(preds, diversity)
				next_char = indices_names_char[next_index]

				sentence = sentence[1:] + next_char

				sys.stdout.write(next_char)
				sys.stdout.flush()
			print()

if __name__=='__main__':
	StepRNNCreation()

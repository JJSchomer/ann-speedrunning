import aiohttp
import asyncio
import time

"""
Based off of Live Python's async tutorial: https://www.youtube.com/watch?v=5tWIxBcvy10&t=455s
"""

async def download_recipe(ArchiveNumber):
	
	print('Started downloading ',ArchiveNumber)
	url='removedforreasons' + str(ArchiveNumber) +'/'
	print('url: ',url)
	async with aiohttp.ClientSession() as session, session.get(url) as resp:
		#async with session.get(URL(url, encoded=True)) as resp:
		content = await resp.read()
		print('Finished downloading ',ArchiveNumber)
		return content
	

async def write_file(ArchiveNumber, content):
	#Find the title of the article to eliminate non-recipes
	title_location = content.find(bytes('<title>', 'utf-8'))
	title_end = content.find(bytes('</title>', 'utf-8'))
	title=str(content[(title_location+7):title_end])[2:-1]
		
	#print('title: ',title)

	if title=='File Not Found':
		print('not a recipe')
	else:
		filename = 'RawData\sync_'+ str(ArchiveNumber) + '.html'
		with open(filename, 'wb') as file:
			print('Started writing ', filename)
			file.write(content)
			print('Finished writing ',filename)

async def scrape_task(ArchiveNumber):
	content = await download_recipe(ArchiveNumber)
	#print('content?',content)
	await write_file(ArchiveNumber, content)

async def main():
	tasks=[]
	for ArchiveNumber in range(10):
		tasks.append(asyncio.ensure_future(scrape_task(ArchiveNumber)))
	print('tasks:', tasks)
	await asyncio.wait(tasks)


if __name__ == '__main__':
	t=time.perf_counter()
	asyncio.run(main())
	#loop=asyncio.get_event_loop()
	#loop.run_until_complete(main())
	t2 = time.perf_counter()-t
	print('Total time elapsed: ',t2)
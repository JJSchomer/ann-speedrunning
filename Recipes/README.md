# Recipe Generation

For my first project, I'll be creating a fun tool that I first had the idea for in early 2019. The idea is to obtain a large database of recipes, then use this database to generate recipes artificially. As an added part of the fun, I plan on actually cooking the most interesting outcomes.

This project can be broken into a few different components:

1. Data collection: There are a wealth of online recipe websites that exist, for the purposes of this exercise I will simply pick one where the actual recipe (not the story behind it) is easy to parse. 

2. Pipelining: Once data is collected, there needs to be some work to ensure that recipes can be fed into a ANN in a convenient form

3. Recurrent Neural Network: There are a number of architectures that should work for actually generating meaningful recipes. I'll start by using a RNN, since they are used often in Natural Language Processing text generators.


Edited- originally I was doing some web-scraping to do data collection, but have now learned that that is frowned upon by the website that I was originally looking at. Instead, I will be using a dataset hosted on kaggle. The code that was used to do the scraping will remain for posterity, but I recommend either using a premade dataset where available or more careful adherence to the scraping policies of the site whose data you are interested in.

This now uses the data collected by shuyangli94 on [kaggle](https://www.kaggle.com/shuyangli94/food-com-recipes-and-user-interactions)  For our purposes, we are mostly interested in the raw recipes, in terms of naming and in terms of the actual step content.

The initial Reccurent ANN is made using the example [lstm text generator from keras as a base](https://keras.io/examples/lstm_text_generation/).


# Initial Network and Characterization

Getting the name generator to function properly was fairly easy, but its more complicated with the step
generator on account of:

1. Problem Complexity: There are more possible configurations for steps, including about twice the number of
characters and many more possible words. There are also many more valid word combinations

2. Input Data Size: The total length of the steps/names combined files is 124 million characters. On the current
hardware, it is intractable to hold that in RAM, and also intractable to pre-slice the data and store it on the 
disk.

This culminates in a relatively modest network (40 input neurons, 124 LSTM neurons, 65 dense neurons) taking about 10 hours to run a single epoch. Normally, I would run a taguchi style characterization matrix varying
the LSTM layer size, learning rate, and input layer size (these are the hyperparameters that should matter the most here), but we're looking at possibly weeks of training time per network.

We're still going to do a sweep, but we're going to do so on a smaller size of the network. We shuffled the data
when we created the database so a pass over enough data should be statistically representative, and should at least point us in the direction of good enough hyperparameters.

Each slice of the Step master file is 2000 characters, so we'll use 100 slices for a total of 200000 characters
in the corpus. A quick check of 200 LSTM neurons and 200 input neurons (chosen based on hardware limitations) yields a per epoch time of about 140 seconds while consuming about half the available RAM, half of the CPU, and 40% of the GPU, giving us some margin to keep doing other work while this goes on (necesary because of working from home due to the Coronavirus).

# First Hyperparameter Sweep

To start our hyperparameter sweep, we'll define our parameters and levels:

1. Input Neurons: 50, 100, 150, 200

2. LSTM Neurons: 50, 100, 150, 200

3. Learning Rate: 0.1, 0.01, 0.001, 0.0001

Where if we see significant performance benefits on outlier values, we'll do another hyperparameter sweep, moving the median to that outlier.

Our taguchi matrix is described as follows (Indexed from 1 by taguchi convention):

1	1	1

1	2	2

1	3	3

1	4	4

2	1	2

2	2	1

2	3	4

2	4	3

3	1	3

3	2	4

3	3	1

3	4	2

4	1	4

4	2	3

4	3	2

4	4	1


for a total of 16 runs. Training time should be bounded by 140 seconds, which means that for 30 epochs per network we can expect a total time cost of 2800 seconds, meaning a global cost of about 20 hours to run the
hyperparameter sweep, which is an acceptable amount of time.

# First Hyperparameter Test Results

The main takeaway from the first hyperparameter sweep was setting an idealized learning rate near 0.01. Every loss-epoch profile was consistent with a 'good' learning pattern, and the two best performing networks after 30 epochs were the two densest at that learning rate. 0.1 failed to converge in some cases, and the two lower LRs had significant flat regions to their learning curves.

The second takeway was the higher relative performance of the denser networks, which suggests that we're still underfitting to the data significantly. This isn't surprising, again remembering that we chose modest networks as a result of hardware limitations, but now that we've dialed in a useful learning rate we're free to try and do more targeted sweeping of network size with constant, likely to work LRs.

There also appears to be a memory leak issue with the generator between epochs- instead of loading 30 epochs of the same data, we'll instead compensate by doing a 15 epochs of double data.

# Second Hyperparameter Sweep

Before we do our second hyperparameter sweep, we need to determine our maximum size before crashing.

There's probably a way to do this properly, but our run-time on the first few operations is fast enough that we can just do a quick binary search to find our max size at about 240 x 240 input/LSTMs

We know that our final dense layer has to have 65 neurons from our target character set. This scales O(number of LSTMS), which scales O(number of input neurons), meaning that we can reasonably expect our size to be equally dependent on those layers. Thus, our size constraint should be ~56000 for our multiple of those two layers, giving us a basis for developing our next parameter sweep:

1. Input Neurons: 100, 190, 240, 300, 560

2. LSTM Neurons: 560, 300, 240, 190, 100

and our parameter matrix is:

1 1

2 2

3 3 

4 4

5 5


There was a very clear trend towards more LSTMs being the best at learning, so we now make a decision for the sake of brevity: The final network will have 50 inputs and 500 LSTMs, and we'll train it for a couple of weeks or so to see where it ends up with occasional checks in terms of generated data to see what we've come up with.

This size network with the amount of data we have takes about 10 hours to run through an epoch, and there's still a persistent memory leak issues with the network (going to try to use a different version of TF for the
next problem). My hope is that after about 20 epochs, we start seeing results reminiscent of actual data that we might find online, which I will consider to be close enough to a success for now.


# Stacking LSTM Layers

After a few days of training, it isn't clear that we can improve substantially beyond 0.68 on the RMSprop loss. We could examine slightly different architectures that try to maximize our LSTM layer size, but an alternative play is to simply deepen our architecture. This provides more abstraction to our model, but we can again take some insight from our previous hyperparameter sweeps to minimize this hyperparameter study time.

We're going to do 10 epochs of 200 examples, and we're going to make sure that we're returning sequences in all but the topmost LSTM layer. We'll follow the old staples about layer sizing- namely that we want decreasing layer size as we move upwards. For now we'll assume that three stacked layers of LSTMS is enough, and that there isn't significant benefit to a deeper fully-connected architecture at the end (I've noticed similar results from previous convolutional network studies that I've run).

We're keeping a LR of 0.01, and keeping input neurons at 50. Structuring the rest of the Sweep is difficult- we need to balance our size constraint of 56000 with increasing layers, meaning the main aspect we should be trading in this study is overall depth.

1.  Input Neurons: 50

2.  Learning Rate: 0.01

One LSTM Layer: 500 (50x500 + 500x65 = 57500)

Two LSTM Layers: 200, 180 (50x200 + 200x180 + 180x65 ~ 57500)

Three LSTM Layers: 180, 140, 110 (50x180 + 180x140 + 140x110+ 110x65 ~57500)

Four LSTM Layers: 150, 130, 120, 80 (50x150 + 150x130 + 130x120 + 120x80 + 80x65 ~57500)

After these tests, there isn't a clear benefit to deeper layer using the same approximate network strength (which makes sense given the inherent depth provided by an LSTM layer).

# Statefulness and Network Size Increasing

The two obvious parameters left to tune are keeping statefulness between samples and taking the hit on training time associated with un-batching the samples.  Statefulness is easy to test so we do that first; unfortunately, there was only a small percieved benefit to statefulnesse.

This brings up the last tool in the arsenal- increasing network complexity. The discerning reader will note that this comes at approximately an n^2 penalty to training time (deeper networks mean more computational resources required for each training sample, and a smaller batch size means more samples per epoch), hence why I prefer it only as a last resort. That being said, we did set a very generous batch size (~650), so we have considerable room to play with here.

We start with a 10X increase in network size, using first one layer and second two LSTM layers (with statefulness since there isn't an associated penalty). Note that this does require substantial changes to our getitem functions

# String Encoding Instead of Character Encoding

After a few weeks of trial and error, there are a lot of persistent errors that can't be rectified. While it is certainly possible that with enough training and data we could make a working model, there's simply too little information encoded into the character representation of the data for reliable feature extraction. By the nature of what we're doing, its necessary to not only character placement, but intelligent grammatical rules and word relationships, all without ever explicitly defining words or sentences.  As such, moving to a string based encoding; while it will certainly increase the encoded data size, should allow a more natural progression into grammer and semantics.

Here is where we need to be careful with encoding in order to make sure we're efficiently storing data.

There are a number of ways we can go about this- but first we need to dive into the data a bit and see what we're working with from a sub-string standpoint:

Tokenizing by string reveals about 60k unique strings, where most of them are spelling mistakes that only occur a few times in the corpus. We can go one-hot if we want at this point, shoving each unique string into its place and hoping that the dataset will give us what we want, but there's still a lot to be gained in terms of preprocessing and cleanup so we're going to spend some time here.

# Problem Formulation and Exploration

We want to generate all the steps of a recipe from just the name of that recipe; we can greatly reduce network size and training time by laying down some ground rules here and constraining our problem/adding in a bit more information.

Lets start with the basics of a recipe- it consists of executing steps using ingredients to end up with a finished meal. Every recipe has an ingredients list, name, and steps (in addition to other parameters that I'm less interested in). By mapping ingredients from the name, then steps from the ingredient/name, we can expect better results than by failing to account for those parameters in each generative pass.

Thus, we want at minimum 3 networks- one for name generation, one for ingredient generation based on name, and one for step generation based on the combination of name and ingredient list. The name generator is still character based which is probably fine- ingredients need to be tokenized and examined next

# Ingredient Data

Looking at the ingredient data on a per-word basis yields some interesting results- there are about 20k unique words that show up in the ingredients lists, of which more than half are only used once and a great number of which are gibberish. On the high end, the first 70 ingredients are all used in more than 10k recipes; meaning we can expect more than a 70 to 1 likelihood of the appearance of one of those most used ingredients in a recipe over any from the bottom half.
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
import time 
import random
import tensorflow as tf
tf.keras.backend.clear_session()
import tensorflow.keras.backend as K
from tensorflow import keras
from tensorflow.keras import Sequential
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dense
from tensorflow.keras.callbacks import LambdaCallback
from tensorflow.keras.utils import Sequence
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.models import model_from_json
import numpy as np
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import h5py
import sys
config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
plt.tight_layout()


char_names_indices = {}
indices_names_char = {}

with h5py.File('EncodedSteps.h5', 'r') as hf:
	for attribute in hf['OneHotSteps'].attrs:
		char_names_indices[str(attribute)]=hf['OneHotSteps'].attrs[str(attribute)]
		indices_names_char[hf['OneHotSteps'].attrs[str(attribute)]]=str(attribute)

char_names_indices.pop('title')
indices_names_char.pop('EncodedRecipeSteps from Food.com')

print('Charinds: ',char_names_indices)
print('IndChars: ',indices_names_char)

Namelength=2000 #derived from earlier function- could also get adaptively from h5py file
Charlength=38
PaddingLength=20

#LSTMs are hungry- don't go overboard here
model = Sequential()
model.add(LSTM(2000, input_shape=(PaddingLength, Charlength)))
model.add(Dense(Charlength, activation='softmax'))

try:
	print('attempting to load weights')
	model.load_weights('model.h5')
	print('load successful')
except:
	print('no weights to load- please use a built model')
	sys.exit()

optimizer = RMSprop(learning_rate=0.1)
model.compile(loss='categorical_crossentropy', optimizer=optimizer)


def sample(preds, temperature=1.0):
	# helper function to sample an index from a probability array
	preds = np.asarray(preds).astype('float64')
	preds = np.log(preds) / temperature
	exp_preds = np.exp(preds)
	preds = exp_preds / np.sum(exp_preds)
	probas = np.random.multinomial(1, preds, 1)
	return np.argmax(probas)


while True:
	"""
	Moved out of main training function since this as a standalone is the goal
	"""

	print()
	print('----- Generating text')

	for textgenCount in range(10):
		print('TextGen #: ',textgenCount)
		with h5py.File('EncodedSteps.h5', 'r') as hf:
			d1_names=hf['OneHotSteps']
			start_index = random.randint(0, len(d1_names))

			sentence1=d1_names[start_index]
			print('sentence1: ',len(sentence1),len(sentence1[0]))
			sentence=''

			for t in range(len(sentence1)):
				for j in range(len(sentence1[t])):
					if sentence1[t][j]==True:
						sentence=sentence+indices_names_char[j]


		for diversity in [0.1, 0.5, 0.8, 1.2]:
			print('----- diversity:', diversity)

			generated = ''
			
			generated += sentence
			print('----- Generating with seed: "' + sentence + '"')
			sys.stdout.write(generated)

			for i in range(PaddingLength+Namelength):
				
				x_pred = np.zeros((1, PaddingLength, len(char_names_indices))) 
				
				while len(sentence)<PaddingLength:
					sentence=sentence+' '+sentence

				for t in range(PaddingLength):
					x_pred[0,t,char_names_indices[sentence[t]]]= True
				
				#for t, char in enumerate(sentence):
					#x_pred[0,t,char_names_indices[char]] = True

				preds = model.predict(x_pred, verbose=0)[0]
				next_index = sample(preds, diversity)
				next_char = indices_names_char[next_index]

				sentence = sentence[1:] + next_char


				sys.stdout.write(next_char)
				sys.stdout.flush()
			print(gencheck)


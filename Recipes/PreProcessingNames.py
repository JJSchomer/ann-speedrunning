import numpy as np
import random
import sys
import io
import pandas as pd
import time
import matplotlib.pyplot as plt
import h5py
import random

"""
Since I'm running this on a fairly modest machine (8GB RAM, 4Cores 3.4GHz,
NVIDIA GeForce GTX 750 Ti), I need to do a lot of the heavy lifting in 
data processing rather than in the neural network itself.

This is where generators come into play- we don't want to load all of our data
into the machine at the same time for risk of crashing, so we schedule it to only
give the machine enough for an epoch.

We want One-Hot Encoded data, and there's no reason to repeat that step every time we
initialize the network. We also want our data in h5py form if we can have it that way, because
that format plays very nicely with generators
"""
t=time.perf_counter()

RecipeRawData=pd.read_csv('RawData/RAW_recipes.csv')
RecipeNames=RecipeRawData.name.tolist()
print('Data Imported')
"""
First things first- we want to visualize our data so we can slice it up
intelligently and determine just what we feed forward as input
"""


maxlen=0
for i in range(len(RecipeNames)):
	RecipeNames[i]=str(RecipeNames[i])
	RecipeNames[i]=RecipeNames[i].lower()
	while "  " in RecipeNames[i]:
		RecipeNames[i]=RecipeNames[i].replace("  ", " ")

	if len(RecipeNames[i])>maxlen:
		maxlen=len(RecipeNames[i])

maxlen+=1
Distribution=np.zeros((maxlen,))

for i in range(len(RecipeNames)):
	Distribution[len(RecipeNames[i])]+=1

plt.plot(Distribution)
plt.xlabel('Name Length')
plt.ylabel('Occurences in Data')
plt.title('Recipe Name Length Distribution')
plt.savefig('Distribution Name')
plt.close()

"""
A quick look at the distribution of name sizes shows that its a skewed distribution towards 20.
The kurtosis isn't too bad, but there are essentially no names past 65 and it's probably not worth
carrying the single 84 char name to term. 

Next we look at the actual recipes themselves, which are already conveniently broken into steps
"""
RecipeSteps=RecipeRawData.steps.tolist()
print('Steps imported')
maxlen=0
for i in range(len(RecipeSteps)):
	RecipeSteps[i]=RecipeSteps[i][1:-1].split('\', ')
	
	for j in range(len(RecipeSteps[i])):
		RecipeSteps[i][j]=RecipeSteps[i][j].replace("'","")
		RecipeSteps[i][j]=RecipeSteps[i][j].lower()
		while "  " in RecipeSteps[i][j]:
			RecipeSteps[i][j]=RecipeSteps[i][j].replace("  ", " ")

	if len(RecipeSteps[i])>maxlen:
		maxlen=len(RecipeSteps[i])

maxlen+=1
Distribution=np.zeros((maxlen,))

for i in range(len(RecipeSteps)):
	Distribution[len(RecipeSteps[i])]+=1

plt.plot(Distribution)
plt.xlabel('Name Length')
plt.ylabel('Occurences in Data')
plt.title('Recipe Step Distribution')
plt.savefig('Distribution Steps')
plt.close()

maxlen=0
for i in range(len(RecipeSteps)):
	for j in range(len(RecipeSteps[i])):
		if len(RecipeSteps[i][j])>maxlen:
			maxlen=len(RecipeSteps[i][j])

maxlen+=1
Distribution=np.zeros((maxlen,))

for i in range(len(RecipeSteps)):
	for j in range(len(RecipeSteps[i])):
		Distribution[len(RecipeSteps[i][j])]+=1

plt.plot(Distribution)
plt.xlabel('Name Length')
plt.ylabel('Occurences in Data')
plt.title('Recipe Step Length Distribution')
plt.savefig('Distribution Step Content')
plt.close()

"""
The Step distribution is a little bit less forgiving. We have one recipe that requires 140 steps that
we can cull outright, and one recipe that consists of a single >1000 character step that likewise need to be 
trimmed.

We handle culling at this stage, making sure to cull from both the recipe names and recipe steps at 
the same time.
"""

Culllist=[]
for i in range(len(RecipeNames)):
	if len(RecipeNames[i])<10 or len(RecipeNames[i])>65:
		Culllist.append(i)

for i in range(len(Culllist)):
	ind=Culllist[-1]
	del RecipeNames[ind]
	del RecipeSteps[ind]
	del Culllist[-1]

Culllist=[]

for i in range(len(RecipeSteps)):
	CullBool=False
	for j in range(len(RecipeSteps[i])):
		if len(RecipeSteps[i][j])>200:
			CullBool=True
	if len(RecipeSteps[i])>60:
		CullBool=True
	if CullBool==True:
		Culllist.append(i)

for i in range(len(Culllist)):
	ind=Culllist[-1]
	del RecipeNames[ind]
	del RecipeSteps[ind]
	del Culllist[-1]

print('Outliers culled')

random.shuffle(RecipeNames)

RecipeNames=' '.join(map(str, RecipeNames))

"""
We now have the two corpus bases that we need to generate recipes- the recipe names
and the actual steps associated.

"""

CharsSteps=[]
for i in range(len(RecipeSteps)):
	for j in range(len(RecipeSteps[i])):
		for k in range(len(RecipeSteps[i][j])):
			if RecipeSteps[i][j][k] not in CharsSteps:
				CharsSteps.append(RecipeSteps[i][j][k])
CharsSteps=sorted(CharsSteps)

print(CharsSteps)
print('step chars: ',len(CharsSteps))

CharsNames = sorted(list(set(RecipeNames)))
print('total chars:', len(CharsNames))
print(CharsNames)

char_names_indices = dict((c, i) for i, c in enumerate(CharsNames))
indices_names_char = dict((i, c) for i, c in enumerate(CharsNames))

char_steps_indices = dict((c, i) for i, c in enumerate(CharsNames))
indices_steps_char = dict((i, c) for i, c in enumerate(CharsNames))

print('Charinds: ',char_names_indices)
print('IndChars: ',indices_names_char)


"""
We start by representing each recipe name as a one-hot encoded 65 char string
The first RNN only generates valid recipe names, then we'll use a second net to 
create step sequences after the fact.
"""

#lots of liberty taken here, feel free to change
#namelen = 66
nameOneHotLen=len(CharsNames)
BatchSize=40 #200 chars to a batch
TotalBatches=(len(RecipeNames)-BatchSize-1)//3 #total batches to save
OneHotNames = np.zeros((TotalBatches,BatchSize,nameOneHotLen),dtype=np.bool_)
OneHotNexts = np.zeros((TotalBatches,1,nameOneHotLen),dtype=np.bool_)

for i in range(TotalBatches):
	for j in range(BatchSize):
		OneHotInd=char_names_indices[RecipeNames[i*3+j]]
		OneHotNames[i][j][OneHotInd]=True
	OneHotInd=char_names_indices[RecipeNames[i*3+BatchSize]]
	OneHotNexts[i][0][OneHotInd]=True
	



t2=time.perf_counter()-t
print('Execution Time: ',t2)

h5f = h5py.File('EncodedNames.h5','w')
dset = h5f.create_dataset('OneHotNames',data=OneHotNames)
dset.attrs['title'] = "EncodedRecipeNames from Food.com"
dset2 = h5f.create_dataset('OneHotNexts',data=OneHotNexts)
for i in range(len(char_names_indices)):
	dset.attrs[indices_names_char[i]]=i

t3=time.perf_counter()-t
print('H5 names time: ',t3)

"""
for at in dset.attrs:
	print(at)
	print(dset.attrs[str(at)])
"""

with h5py.File('EncodedNames.h5', 'r') as hf:
	d1_names=hf['OneHotNames']
	for i in range(len(d1_names)):
		x1_true=''
		for t in range(len(d1_names[i])):
			for j in range(len(d1_names[i][t])):
				if d1_names[i][t][j]==True:
					x1_true=x1_true+indices_names_char[j]
		print('true name: ', x1_true)


sys.exit()


"""
We can an LSTM cheat to store the data for steps more efficiently and make it easier
on the recipe generator- Each recipe step is stored as a hash function where the key is 
the recipe name- meaning we can always associate the generated step with the recipe name 
as well as previous steps.


"""
	
print('Total sentences:', len(sentences))

print('example sentences: ',sentences[:100])
print('example next_chars: ',next_chars[:100])


print('One Hot Encoding')
x = np.zeros((len(sentences), maxlen, len(chars)), dtype=np.bool)
y = np.zeros((len(sentences), len(chars)), dtype=np.bool)
for i, sentence in enumerate(sentences):
	for t, char in enumerate(sentence):
		x[i, t, char_indices[char]] = 1
	y[i, char_indices[next_chars[i]]] = 1


print('x shape: ',x.shape)
print('y shape: ',y.shape)

print('x vects: ',x[0,:,:])
print('y vects: ',x[0,:])

#LSTMs are hungry- don't go overboard here
print('Build model...')
model = Sequential()
model.add(LSTM(128, input_shape=(maxlen, len(chars))))
model.add(Dense(len(chars), activation='softmax'))

optimizer = RMSprop(learning_rate=0.01)
model.compile(loss='categorical_crossentropy', optimizer=optimizer)


def sample(preds, temperature=1.0):
	# helper function to sample an index from a probability array
	preds = np.asarray(preds).astype('float64')
	preds = np.log(preds) / temperature
	exp_preds = np.exp(preds)
	preds = exp_preds / np.sum(exp_preds)
	probas = np.random.multinomial(1, preds, 1)
	return np.argmax(probas)


def on_epoch_end(epoch, _):
	# Function invoked at end of each epoch. Prints generated text.
	print()
	print('----- Generating text after Epoch: %d' % epoch)

	start_index = random.randint(0, len(text) - maxlen - 1)
	for diversity in [0.2, 0.5, 1.0, 1.2]:
		print('----- diversity:', diversity)

		generated = ''
		sentence = text[start_index: start_index + maxlen]
		generated += sentence
		print('----- Generating with seed: "' + sentence + '"')
		sys.stdout.write(generated)

		for i in range(400):
			x_pred = np.zeros((1, maxlen, len(chars)))
			for t, char in enumerate(sentence):
				x_pred[0, t, char_indices[char]] = 1.

			preds = model.predict(x_pred, verbose=0)[0]
			next_index = sample(preds, diversity)
			next_char = indices_char[next_index]

			sentence = sentence[1:] + next_char

			sys.stdout.write(next_char)
			sys.stdout.flush()
		print()

print_callback = LambdaCallback(on_epoch_end=on_epoch_end)

model.fit(x, y,
		  batch_size=128,
		  epochs=60,
		  callbacks=[print_callback])

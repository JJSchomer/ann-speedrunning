import os
import time 
import random
import StepsRNN
import xlrd
#First Hyperparameter Sweep:
"""
level4taguchi=[[0,0,0],
				[0,1,1],
				[0,2,2],
				[0,3,3],
				[1,0,1],
				[1,1,0],
				[1,2,3],
				[1,3,2],
				[2,0,2],
				[2,1,3],
				[2,2,0],
				[2,3,1],
				[3,0,3],
				[3,1,2],
				[3,2,1],
				[3,3,0]]

LSTMNeurons=[50,100,150,200]
InputNeurons=[50,100,150,200]
LearningRates=[0.1,0.01,0.001,0.0001]
"""


#Second Hyperparameter Sweep:

level4taguchi=[[0,0,0],
			[1,1,0],
			[2,2,0],
			[3,3,0],
			[4,4,0]]

LSTMNeurons=[2000]
InputNeurons=[20]
LearningRates=[0.001]


dirname= "PostProcessing"
os.chdir(dirname)

#dirname= "LSTM"+str(LSTMNeurons[0])+"LR"+str(LearningRates[0])+"IN"+str(InputNeurons[0])+"model"
dirname='currentModel'
os.chdir(dirname)

wb = xlrd.open_workbook(dirname+'.xlsx')
sheet=wb.sheet_by_index(0)
start_Index=sheet.cell_value(1,2)

start_Index=0
#start_Index=int(start_Index+2000)
end_Index=int(start_Index+2000)

print('start/end: ',start_Index,end_Index)

os.chdir('..')
os.chdir('..')



for Tpara in level4taguchi:
	LSTM_number=LSTMNeurons[Tpara[0]]
	Input_number=InputNeurons[Tpara[1]]
	Learning_rate=LearningRates[Tpara[2]]
	print('params: ',Tpara, LSTM_number, Input_number, Learning_rate)
	StepsRNN.StepRNNCreation(LSTM_number,Input_number,Learning_rate,start_Index=start_Index, end_Index=end_Index, train_epochs=20)

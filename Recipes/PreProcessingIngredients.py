import numpy as np
import random
import sys
import io
import pandas as pd
import time
import matplotlib.pyplot as plt
import h5py
import random
import re


"""
Since I'm running this on a fairly modest machine (8GB RAM, 4Cores 3.4GHz,
NVIDIA GeForce GTX 750 Ti), I need to do a lot of the heavy lifting in 
data processing rather than in the neural network itself.

This is where generators come into play- we don't want to load all of our data
into the machine at the same time for risk of crashing, so we schedule it to only
give the machine enough for an epoch.

We want One-Hot Encoded data, and there's no reason to repeat that step every time we
initialize the network. We also want our data in h5py form if we can have it that way, because
that format plays very nicely with generators
"""
t=time.perf_counter()

RecipeRawData=pd.read_csv('RawData/RAW_recipes.csv')
RecipeNames=RecipeRawData.name.tolist()
print('Data Imported')
"""
First things first- we want to visualize our data so we can slice it up
intelligently and determine just what we feed forward as input
"""


maxlen=0
for i in range(len(RecipeNames)):
	RecipeNames[i]=str(RecipeNames[i])
	RecipeNames[i]=RecipeNames[i].lower()
	while "  " in RecipeNames[i]:
		RecipeNames[i]=RecipeNames[i].replace("  ", " ")

	if len(RecipeNames[i])>maxlen:
		maxlen=len(RecipeNames[i])

maxlen+=1
Distribution=np.zeros((maxlen,))

for i in range(len(RecipeNames)):
	Distribution[len(RecipeNames[i])]+=1

plt.plot(Distribution)
plt.xlabel('Name Length')
plt.ylabel('Occurences in Data')
plt.title('Recipe Name Length Distribution')
plt.savefig('Distribution Name')
plt.close()

"""
A quick look at the distribution of name sizes shows that its a skewed distribution towards 20.
The kurtosis isn't too bad, but there are essentially no names past 65 and it's probably not worth
carrying the single 84 char name to term. 

Next we look at the actual recipes themselves, which are already broken into steps.

Instead of leaving them in this form, we're going to combine steps into a larger 'recipe' string, for 
reasons that will become apparant in the stepsRNN.


Unfortunately, the total recipe length is super skewed. We see a few at 10000, most between about
200 and 300, and quite a few at 2000. For our purposes, we're trimming the 8kish below 100.
"""

RecipeSteps=RecipeRawData.ingredients.tolist()
print('ingredients imported')
maxlen=0
for i in range(len(RecipeSteps)):
	RecipeSteps[i]=RecipeSteps[i][1:-1].split('\', ')
	
	for j in range(len(RecipeSteps[i])):
		#print('j: ',j)
		RecipeSteps[i][j]=RecipeSteps[i][j].replace("'","")
		RecipeSteps[i][j]=RecipeSteps[i][j].lower()
		while "  " in RecipeSteps[i][j]:
			RecipeSteps[i][j]=RecipeSteps[i][j].replace("  ", " ")
		while "-" in RecipeSteps[i][j]:
			RecipeSteps[i][j]=RecipeSteps[i][j].replace("-"," ")
		while ":" in RecipeSteps[i][j]:
			RecipeSteps[i][j]=RecipeSteps[i][j].replace(":"," ")
		while ";" in RecipeSteps[i][j]:
			RecipeSteps[i][j]=RecipeSteps[i][j].replace(";"," ")


greaterthan2000=0
for i in range(len(RecipeSteps)):
	RecipeSteps[i]=' '.join(map(str, RecipeSteps[i]))
	if len(RecipeSteps[i]) > maxlen:
		maxlen=len(RecipeSteps[i])
	if len(RecipeSteps[i]) > 2000:
		greaterthan2000+=1

maxlen+=1
Distribution=np.zeros((maxlen,))

print('greater than 2000: ', greaterthan2000)


for i in range(len(RecipeSteps)):
	Distribution[len(RecipeSteps[i])]+=1

plt.plot(Distribution)
plt.xlabel('ingredient Length')
plt.ylabel('Occurences in Data')
plt.title('Recipe Step Distribution')
plt.savefig('Distribution Steps')
plt.close()



"""
We cull the recipes with too short names, as well as the recipes with too little useful data. This means
the ones with more than 2k chars and with less than 100 chars (if we leave the 10k examples in, they will
dominate the learned space which we don't necessarily want)

"""

#sys.exit()

Culllist=[]
for i in range(len(RecipeNames)):
	if len(RecipeNames[i])<10 or len(RecipeNames[i])>65:
		Culllist.append(i)

for i in range(len(Culllist)):
	ind=Culllist[-1]
	del RecipeNames[ind]
	del RecipeSteps[ind]
	del Culllist[-1]

Culllist=[]

for i in range(len(RecipeSteps)):
	CullBool=False
	if len(RecipeSteps[i])>2000:
		CullBool=True
	if len(RecipeSteps[i])<100:
		CullBool=True
	if CullBool==True:
		Culllist.append(i)

for i in range(len(Culllist)):
	ind=Culllist[-1]
	del RecipeNames[ind]
	del RecipeSteps[ind]
	del Culllist[-1]

print('Outliers culled')

"""
We now have the two corpus bases that we need to generate recipes- the recipe names
and the actual steps associated.

The namegenerator and logic is handled elsewhere- the 

"""
#NewStrings=[]
for i in range(len(RecipeSteps)):
	RecipeSteps[i]=re.sub('[^A-Za-z0-9, ]+', '',RecipeSteps[i])
	#NewStrings.append(RecipeSteps[i].strip("{!\"\'#$%^()+-/[]@?<=>\\^_`|~}:;"))


#RecipeSteps=NewStrings
#del(NewStrings)

CharsSteps=[]
for i in range(len(RecipeSteps)):
	for j in range(len(RecipeSteps[i])):
		if RecipeSteps[i][j] not in CharsSteps:
				CharsSteps.append(RecipeSteps[i][j])
CharsSteps=sorted(CharsSteps)
print(CharsSteps)
print('step chars: ',len(CharsSteps))

char_steps_indices = dict((c, i) for i, c in enumerate(CharsSteps))
indices_steps_char = dict((i, c) for i, c in enumerate(CharsSteps))

print('Charinds: ',char_steps_indices)
print('IndChars: ',indices_steps_char)

"""
Slicing the data here isn't something we can do haphazardly. The biggest issue is that
the data has random name lengths and step lengths, which is problematic for us to handle (we can
store everything heirarchically, but at storage costs that outweight the performance improvements.

We can use some tricks to make the problem tractable- We know that names should always be 
predetermined, so we don't need rolling generation like we did before. Instead of concatenating
everything into one string and letting the RNN go ham, we're going to cut into 65 char chunks that
start with the name and go until they hit the string end. This yields a dataset that's on the large end
but not oppressive (~XXXX bytes ~)

"""
#I prefer shuffling first to minimize tenfsorflow ops, but its a matter of preference
#Probably a way to do this in constant memory, left as an exercise for the reader
randomize = np.arange(len(RecipeSteps))
np.random.shuffle(randomize)
StepPlaceholder=[]
NamePlaceholder=[]
for i in range(len(RecipeSteps)):
	StepPlaceholder.append(RecipeSteps[randomize[i]])
	NamePlaceholder.append(RecipeNames[randomize[i]])

RecipeSteps=StepPlaceholder
RecipeNames=NamePlaceholder

del(StepPlaceholder)
del(NamePlaceholder)

namemax=0
for name in RecipeNames:
	if len(name)>namemax:
		namemax=len(name)

#lots of liberty taken here, feel free to change
nameOneHotLen=len(CharsSteps)

#Combine Data as needed
for i in range(len(RecipeSteps)):
	RecipeSteps[i]=RecipeNames[i]+' '+RecipeSteps[i]


"""
First pass ignores the segmented stratagy that we want employ later-
just start by building a corpus and iterating on it
"""

RecipeSteps=' '.join(map(str, RecipeSteps))
print('countcheck: ',len(RecipeSteps))

counts=dict()
words = RecipeSteps.split()

for word in words:
	if word in counts:
		counts[word]+=1
	else:
		counts[word]=1

df = pd.DataFrame(data=counts, index=[0])
df = (df.T)
print(df)

df.to_excel('ingcounts.xlsx')

sys.exit()

#We aren't saving in batches to save the poor, poor disk
batchsize=2000
TotalBatches=(len(RecipeSteps)//batchsize)
OneHotSteps = np.zeros((TotalBatches, batchsize, nameOneHotLen),dtype=np.bool_)#Save the whole name

print('size OneHots: ',OneHotSteps.shape)


for i in range(TotalBatches):
	for j in range(batchsize):
		OneHotInd=char_steps_indices[RecipeSteps[i*batchsize+j]]
		OneHotSteps[i][j][OneHotInd]=True


t2=time.perf_counter()-t
print('Execution Time: ',t2)

h5f = h5py.File('EncodedSteps.h5','w')
dset = h5f.create_dataset('OneHotSteps',data=OneHotSteps)
dset.attrs['title'] = "EncodedRecipeSteps from Food.com"
for i in range(len(char_steps_indices)):
	dset.attrs[indices_steps_char[i]]=i

t3=time.perf_counter()-t
print('H5 steps time: ',t3)

"""
for at in dset.attrs:
	print(at)
	print(dset.attrs[str(at)])
"""

with h5py.File('EncodedSteps.h5', 'r') as hf:
	d1_names=hf['OneHotSteps']
	for i in range(len(d1_names)):
		x1_true=''
		for t in range(len(d1_names[i])):
			for j in range(len(d1_names[i][t])):
				if d1_names[i][t][j]==True:
					x1_true=x1_true+indices_steps_char[j]
		print('true name: ', x1_true)


sys.exit()


"""
We can an LSTM cheat to store the data for steps more efficiently and make it easier
on the recipe generator- Each recipe step is stored as a hash function where the key is 
the recipe name- meaning we can always associate the generated step with the recipe name 
as well as previous steps.


"""
	
print('Total sentences:', len(sentences))

print('example sentences: ',sentences[:100])
print('example next_chars: ',next_chars[:100])


print('One Hot Encoding')
x = np.zeros((len(sentences), maxlen, len(chars)), dtype=np.bool)
y = np.zeros((len(sentences), len(chars)), dtype=np.bool)
for i, sentence in enumerate(sentences):
	for t, char in enumerate(sentence):
		x[i, t, char_indices[char]] = 1
	y[i, char_indices[next_chars[i]]] = 1


print('x shape: ',x.shape)
print('y shape: ',y.shape)

print('x vects: ',x[0,:,:])
print('y vects: ',x[0,:])

#LSTMs are hungry- don't go overboard here
print('Build model...')
model = Sequential()
model.add(LSTM(128, input_shape=(maxlen, len(chars))))
model.add(Dense(len(chars), activation='softmax'))

optimizer = RMSprop(learning_rate=0.01)
model.compile(loss='categorical_crossentropy', optimizer=optimizer)


def sample(preds, temperature=1.0):
	# helper function to sample an index from a probability array
	preds = np.asarray(preds).astype('float64')
	preds = np.log(preds) / temperature
	exp_preds = np.exp(preds)
	preds = exp_preds / np.sum(exp_preds)
	probas = np.random.multinomial(1, preds, 1)
	return np.argmax(probas)


def on_epoch_end(epoch, _):
	# Function invoked at end of each epoch. Prints generated text.
	print()
	print('----- Generating text after Epoch: %d' % epoch)

	start_index = random.randint(0, len(text) - maxlen - 1)
	for diversity in [0.2, 0.5, 1.0, 1.2]:
		print('----- diversity:', diversity)

		generated = ''
		sentence = text[start_index: start_index + maxlen]
		generated += sentence
		print('----- Generating with seed: "' + sentence + '"')
		sys.stdout.write(generated)

		for i in range(400):
			x_pred = np.zeros((1, maxlen, len(chars)))
			for t, char in enumerate(sentence):
				x_pred[0, t, char_indices[char]] = 1.

			preds = model.predict(x_pred, verbose=0)[0]
			next_index = sample(preds, diversity)
			next_char = indices_char[next_index]

			sentence = sentence[1:] + next_char

			sys.stdout.write(next_char)
			sys.stdout.flush()
		print()

print_callback = LambdaCallback(on_epoch_end=on_epoch_end)

model.fit(x, y,
		  batch_size=128,
		  epochs=60,
		  callbacks=[print_callback])

import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
import time 
import random
import tensorflow as tf
tf.keras.backend.clear_session()
import tensorflow.keras.backend as K
from tensorflow import keras
from tensorflow.keras import Sequential
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dense
from tensorflow.keras.callbacks import LambdaCallback
from tensorflow.keras.utils import Sequence
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.models import model_from_json
import numpy as np
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import h5py
import sys
config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
plt.tight_layout()


"""
We want to predict the next character given several characters. For our purposes,
we'll use a predictionlength of 20, and pad each recipe name by 20 zeroes front and back

From each call to NameGenerator()
"""

class NameGenerator(Sequence):
	
	"""
	By padding the input, we should be able to start learning to generate concise recipe names.

	What actually happens is that we create a significant bias towards spaces. One way to combat
	this is to artificially truncate our data. We want the same step size, but we don't want a bunch
	of empty spaces polluting the trainability, so we can wrap text instead.
	"""

	def __init__(self, file_name='EncodedNames.h5', batch_size=50, start_Index=0,
					 end_Index=None, PaddingLength=40):
		#Only using one file for now
		self.hf=h5py.File(file_name, 'r')
		NamesAll=self.hf['OneHotNames'][:]
		self.truelen=len(NamesAll)
		self.batch_size = batch_size
		self.NamesLen=len(self.hf['OneHotNames'][0])#check
		print('NamesLen ',self.NamesLen)
		self.PaddingLength=PaddingLength

		if end_Index==None:
			self.total_len=len(NamesAll) - start_Index*batch_size
			self.total_len=self.total_len-self.total_len%batch_size
		else:
			self.total_len = end_Index*batch_size - start_Index*batch_size
		self.idx = start_Index

		self.char_names_indices = {}
		self.indices_names_char = {}

		for attribute in self.hf['OneHotNames'].attrs:
			self.char_names_indices[str(attribute)]=self.hf['OneHotNames'].attrs[str(attribute)]
			self.indices_names_char[self.hf['OneHotNames'].attrs[str(attribute)]]=str(attribute)

		#A bit sloppy but works
		self.char_names_indices.pop('title')
		self.indices_names_char.pop('EncodedRecipeNames from Food.com')
		self.Charlength=len(self.char_names_indices)
		print('Charlength: ',self.Charlength)

	def __len__(self):
		return int(self.total_len//self.batch_size)

	def __getitem__(self, idx):
		mod = 0
		if self.idx!=0:
			mod = self.idx

		"""
		We need to break individual names into multiple samples
		"""
		batch_y=[]#np.ndarray([])
		batch_x=[]#np.ndarray([])
		#self.blanks[:,0]=True


		#if (idx+1+mod)*self.batch_size <= self.truelen-batch:
			
		for i in range( (idx+mod)*self.batch_size, (idx+mod+1)*self.batch_size ) :
				
			batch_x.append(self.hf['OneHotNames'][i])
			batch_y.append(self.hf['OneHotNexts'][i][0])

		batch_y=np.array(batch_y)
		batch_x=np.array(batch_x)

		#print('batch_y len: ',len(batch_y))
		#print('batch_x len: ',len(batch_x))

		return(batch_x, batch_y)
	

char_names_indices = {}
indices_names_char = {}

with h5py.File('EncodedNames.h5', 'r') as hf:
	for attribute in hf['OneHotNames'].attrs:
		char_names_indices[str(attribute)]=hf['OneHotNames'].attrs[str(attribute)]
		indices_names_char[hf['OneHotNames'].attrs[str(attribute)]]=str(attribute)

char_names_indices.pop('title')
indices_names_char.pop('EncodedRecipeNames from Food.com')

print('Charinds: ',char_names_indices)
print('IndChars: ',indices_names_char)


#Namelength=66 #derive more intelligently later
#Charlength=len(char_names_indices)

#RecipeRawData=pd.read_csv('RawData/RAW_recipes.csv')
#RecipeNames=NameGenerator(file_name='EncodedNames.h5', batch_size=1)
batch_size=50
end_Index=None
RecipeNames=NameGenerator(file_name='EncodedNames.h5', batch_size=batch_size, end_Index=end_Index)

Namelength=RecipeNames.NamesLen
Charlength=RecipeNames.Charlength
PaddingLength=RecipeNames.PaddingLength

#LSTMs are hungry- don't go overboard here
model = Sequential()
model.add(LSTM(128, input_shape=(PaddingLength, Charlength)))
model.add(Dense(Charlength, activation='softmax'))

try:
	print('attempting to load weights')
	model.load_weights('model.h5')
	print('load successful')
except:
	print('no weights to load')
	pass

	

optimizer = RMSprop(learning_rate=0.001)
model.compile(loss='categorical_crossentropy', optimizer=optimizer)


def sample(preds, temperature=1.0):
	# helper function to sample an index from a probability array
	preds = np.asarray(preds).astype('float64')
	preds = np.log(preds) / temperature
	exp_preds = np.exp(preds)
	preds = exp_preds / np.sum(exp_preds)
	probas = np.random.multinomial(1, preds, 1)
	return np.argmax(probas)


def on_epoch_end(epoch, _):
	# Function invoked at end of each epoch. Prints generated text.
	
	print('saving weights')
	model.save_weights("model"+str(epoch)+".h5")
	print('model saved to disk')


	print()
	print('----- Generating text after Epoch: %d' % epoch)

	for textgenCount in range(10):
		print('TextGen #: ',textgenCount)
		with h5py.File('EncodedNames.h5', 'r') as hf:
			d1_names=hf['OneHotNames']
			start_index = random.randint(0, len(d1_names))

			sentence1=d1_names[start_index]
			print('sentence1: ',len(sentence1),len(sentence1[0]))
			sentence=''

			for t in range(len(sentence1)):
				for j in range(len(sentence1[t])):
					if sentence1[t][j]==True:
						sentence=sentence+indices_names_char[j]


		for diversity in [0.5, 0.6, 0.8, 0.9]:
			print('----- diversity:', diversity)

			generated = ''
			
			generated += sentence
			print('----- Generating with seed: "' + sentence + '"')
			sys.stdout.write(generated)

			for i in range(RecipeNames.PaddingLength+RecipeNames.NamesLen):
				
				x_pred = np.zeros((1, RecipeNames.PaddingLength, len(char_names_indices))) 
				
				while len(sentence)<RecipeNames.PaddingLength:
					sentence=sentence+' '+sentence

				for t in range(RecipeNames.PaddingLength):
					x_pred[0,t,char_names_indices[sentence[t]]]= True
				
				
				#for t, char in enumerate(sentence):
					#x_pred[0,t,char_names_indices[char]] = True

				preds = model.predict(x_pred, verbose=0)[0]
				next_index = sample(preds, diversity)
				next_char = indices_names_char[next_index]

				sentence = sentence[1:] + next_char

				sys.stdout.write(next_char)
				sys.stdout.flush()
			print()

print_callback = LambdaCallback(on_epoch_end=on_epoch_end)

model.fit(RecipeNames,
		epochs=60,
		callbacks=[print_callback],
		shuffle=False,
		steps_per_epoch = None,
		use_multiprocessing = False,
		verbose=1,
		max_queue_size=100,
		workers=10)
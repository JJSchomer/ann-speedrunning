import requests
import time


def download_recipe(ArchiveNumber):
	print('Started downloading ',ArchiveNumber)
	url='removedforreasons' + str(ArchiveNumber) +'/'
	response=requests.get(url)
	print('Finished downloading ',ArchiveNumber)
	return response.content


def write_file(ArchiveNumber, content):
	#Find the title of the article to eliminate non-recipes
	title_location = content.find(bytes('<title>', 'utf-8'))
	title_end = content.find(bytes('</title>', 'utf-8'))
	title=str(content[(title_location+7):title_end])[2:-1]
	
	if title=='File Not Found':
		print('not a recipe')
	else:
		filename = 'RawData\sync_'+ str(ArchiveNumber) + '.html'
		with open(filename, 'wb') as file:
			print('Started writing ', filename)
			file.write(content)
			print('Finished writing ',filename)

if __name__ == '__main__':
	t=time.perf_counter()
	for ArchiveNumber in range(10000,12001):
		#for n, url in enumerate(open('urls.txt').readlines()):
		content = download_recipe(ArchiveNumber)
		write_file(ArchiveNumber,content)
		t2 = time.perf_counter()-t
		print('Total time elapsed: ',t2)